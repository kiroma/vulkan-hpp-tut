#version 460 core
layout(location = 0) in vec3 inCol;
layout(location = 1) in vec2 texCoord;
layout(location = 0) out vec4 outCol;

layout(binding = 1) uniform sampler2D texSampler;

void main()
{
	outCol = vec4(inCol * texture(texSampler, texCoord).rgb, 1);
}
