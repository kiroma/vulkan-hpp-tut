#version 460 core
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 outCol;
layout(location = 1) out vec2 outTexCoord;

layout(binding = 0, std140) uniform UBO {
	mat4 model;
	mat4 view;
	mat4 projection;
};

layout(constant_id = 0) const float fovy = radians(90);
layout(constant_id = 1) const float aspect = 16.0/9.0;
layout(constant_id = 2) const float zNear = 1.0/16.0;

mat4 infinitePerspectiveRH(float fovy, float aspect, float znear)
{
	float f = 1.0f/tan(fovy / 2.0f);
	return mat4(
		f / aspect, 0, 0, 0,
		0, -f, 0, 0,
		0, 0, 0, -1.0f,
		0, 0, znear, 0);
}

void main()
{
	gl_Position = infinitePerspectiveRH(fovy, aspect, zNear) * view * model * vec4(inPosition, 1.0);
	outCol = inColor;
	outTexCoord = inTexCoord;
}
