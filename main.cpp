#ifndef VULKAN_HPP_DISPATCH_LOADER_DYNAMIC
	#define VULKAN_HPP_DISPATCH_LOADER_DYNAMIC = 1
#endif

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <GLFW/glfw3.h>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <map>
#include <numeric>
#include <optional>
#include <set>
#include <stdexcept>
#include <string_view>
#include <utility>
#include <vector>
#include <vulkan/vulkan.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

std::string readFile(const std::filesystem::path& filepath)
{
	std::ifstream ifs(filepath, std::ios::in | std::ios::binary);
	std::size_t size = std::filesystem::file_size(filepath);
	std::string data(size, 0);
	ifs.read(data.data(), data.size());
	return data;
}

VULKAN_HPP_DEFAULT_DISPATCH_LOADER_DYNAMIC_STORAGE

const uint32_t WIDTH = 800, HEIGHT = 600;

struct Vertex
{
	glm::vec3 pos;
	glm::vec3 color;
	glm::vec2 texCoord;
	static constexpr vk::VertexInputBindingDescription getBindingDescription() noexcept
	{
		vk::VertexInputBindingDescription bindingDescription(
			0,
			sizeof(Vertex),
			vk::VertexInputRate::eVertex);
		return bindingDescription;
	}
	static constexpr auto getAttributeDescriptions() noexcept
	{
		constexpr std::array attributeDescriptions = {
			vk::VertexInputAttributeDescription(
				0,
				0,
				vk::Format::eR32G32B32Sfloat,
				offsetof(Vertex, pos)),
			vk::VertexInputAttributeDescription(
				1,
				0,
				vk::Format::eR32G32B32Sfloat,
				offsetof(Vertex, color)),
			vk::VertexInputAttributeDescription(
				2,
				0,
				vk::Format::eR32G32Sfloat,
				offsetof(Vertex, texCoord)),
		};
		return attributeDescriptions;
	}
};

struct UniformBufferObject
{
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 projection;
};

// struct PushConstants
// {
// 	glm::mat4 view;
// 	glm::mat4 projection;
// };
//
// static_assert(sizeof(PushConstants) <= 128, "Assume we've only got 128 bytes of push constants available");

static constexpr std::array vertices = {
	Vertex{{-0.5f, -0.5f, 0.0f},	 {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
	Vertex{{0.5f, -0.5f, 0.0f},	{0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
	Vertex{{0.5f, 0.5f, 0.0f},	   {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
	Vertex{{-0.5f, 0.5f, 0.0f},	{1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},

	Vertex{{-0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
	Vertex{{0.5f, -0.5f, -0.5f},	 {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
	Vertex{{0.5f, 0.5f, -0.5f},	{0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
	Vertex{{-0.5f, 0.5f, -0.5f},	 {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},
};

static constexpr std::array<uint32_t, 12> indices = {
	0, 1, 2, 2, 3, 0,
	4, 5, 6, 6, 7, 4};

class HelloTriangle
{
	GLFWwindow* window;
	vk::DynamicLoader dl;
	vk::Instance instance;
#ifndef NDEBUG
	vk::DebugUtilsMessengerEXT debugMessenger;
#endif

	static constexpr std::array validationLayers =
		{
			"VK_LAYER_KHRONOS_validation",
	};

	static constexpr std::array deviceExtensions =
		{
			VK_KHR_SWAPCHAIN_EXTENSION_NAME,
	};

	void createInstance()
	{
		vk::ApplicationInfo appInfo = vk::ApplicationInfo(
			"Hello Triangle",
			VK_MAKE_VERSION(1, 0, 0),
			"No Engine",
			VK_MAKE_VERSION(1, 0, 0),
			VK_API_VERSION_1_2);

		uint32_t glfwExtensionCount;
		const char* const* const glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
		std::vector extensions = std::vector(glfwExtensions, glfwExtensions + glfwExtensionCount);
#ifndef NDEBUG
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif

		VULKAN_HPP_DEFAULT_DISPATCHER.init(dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr"));

		std::vector layers = vk::enumerateInstanceLayerProperties();

		if (!std::all_of(validationLayers.begin(), validationLayers.end(), [&layers](const std::string_view requiredLayer) {
				return std::find_if(layers.begin(), layers.end(), [&requiredLayer](vk::LayerProperties layer) {
					return requiredLayer == layer.layerName;
				}) != layers.end();
			}))
		{
			throw std::runtime_error("Could not find a requested layer");
		}

		constexpr std::array enabledFeatures = {
			vk::ValidationFeatureEnableEXT::eGpuAssisted,
			vk::ValidationFeatureEnableEXT::eGpuAssistedReserveBindingSlot,
			// 			vk::ValidationFeatureEnableEXT::eBestPractices,
			vk::ValidationFeatureEnableEXT::eSynchronizationValidation,
		};

		vk::StructureChain createInfo{
			vk::InstanceCreateInfo(
				{},
				&appInfo,
#ifdef NDEBUG
				{},
				{},
#else
				validationLayers.size(),
				validationLayers.data(),
#endif
				extensions.size(),
				extensions.data()),
#ifndef NDEBUG
			vk::DebugUtilsMessengerCreateInfoEXT(
				{},
				vk::DebugUtilsMessageSeverityFlagBitsEXT::eError | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning,
				vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation,
				&debugCallback),
			vk::ValidationFeaturesEXT(
				enabledFeatures,
				{}),
#endif
		};

		instance = vk::createInstance(std::get<0>(createInfo));

		VULKAN_HPP_DEFAULT_DISPATCHER.init(instance);
	}

	static VKAPI_PTR VkBool32 debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverityArg,
		VkDebugUtilsMessageTypeFlagsEXT messageTypeArg,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackDataArg,
		void*)
	{
		auto messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT(messageSeverityArg);
		auto messageType = vk::DebugUtilsMessageTypeFlagsEXT(messageTypeArg);
		auto callbackData = vk::DebugUtilsMessengerCallbackDataEXT(*pCallbackDataArg);

		std::ostream& output = messageSeverity > vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo ? std::cerr : std::clog;
		output << vk::to_string(messageSeverity) << ": " << vk::to_string(messageType) << ":\n"
			   << "\tmessageIDName = " << callbackData.pMessageIdName << '\n'
			   << "\tmessageIDNumber = " << callbackData.messageIdNumber << '\n'
			   << "\tmessage = " << callbackData.pMessage << '\n';
		if (callbackData.queueLabelCount > 0)
		{
			output << "\tQueue Labels:\n";
			for (unsigned i = 0; i < callbackData.queueLabelCount; ++i)
			{
				output << "\t\tlabelName = " << callbackData.pQueueLabels[i].pLabelName << '\n';
			}
		}
		if (callbackData.cmdBufLabelCount > 0)
		{
			output << "\tCommandBuffer Labels:\n";
			for (unsigned i = 0; i < callbackData.cmdBufLabelCount; ++i)
			{
				output << "\t\tlabelName = " << callbackData.pCmdBufLabels[i].pLabelName << '\n';
			}
		}
		if (callbackData.objectCount > 0)
		{
			output << "\tObjects:\n";
			for (unsigned i = 0; i < callbackData.objectCount; ++i)
			{
				output << "\t\tObject " << i << ":\n";
				output << "\t\t\tObject Type = " << vk::to_string(static_cast<vk::ObjectType>(callbackData.pObjects[i].objectType)) << '\n';
				output << "\t\t\tObject Handle = " << callbackData.pObjects[i].objectHandle << '\n';
				if (callbackData.pObjects[i].pObjectName)
				{
					output << "\t\t\tObject Name = " << callbackData.pObjects[i].pObjectName << '\n';
				}
			}
		}
		return VK_FALSE;
	}
	void setupDebugMessenger()
	{
#ifndef NDEBUG
		auto createInfo = vk::DebugUtilsMessengerCreateInfoEXT(
			{},
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eError | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo,
			vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation,
			&debugCallback);
		debugMessenger = instance.createDebugUtilsMessengerEXT(createInfo);
#endif
	}

	vk::SurfaceKHR surface;

	void createSurface()
	{
		VkSurfaceKHR surface;
		if (glfwCreateWindowSurface(instance, window, nullptr, &surface))
		{
			throw std::runtime_error("Failed to create window surface!");
		}
		this->surface = surface;
	}

	void initWindow()
	{
		glfwInit();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
		window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan Window", nullptr, nullptr);
		glfwSetErrorCallback([](int errorID, const char* message) {
			std::cerr << "GLFW Error " << errorID << ": " << message << '\n';
		});
		glfwSetWindowUserPointer(window, this);
		glfwSetFramebufferSizeCallback(window, [](GLFWwindow* window, int, int) {
			auto* ptr = static_cast<HelloTriangle*>(glfwGetWindowUserPointer(window));
			ptr->frameBufferResized = true;
		});
	}

	struct queueFamilyIndices
	{
		std::optional<uint32_t> graphicsFamily;
		std::optional<uint32_t> presentFamily;
		std::optional<uint32_t> transferFamily;

		constexpr operator bool() const
		{
			return graphicsFamily.has_value() && presentFamily.has_value() && transferFamily.has_value();
		}
	};
	queueFamilyIndices queueIndices;

	template<typename T>
	static constexpr uint32_t popcnt(T in)
	{
		auto t = VkFlags(in);
		uint32_t result = 0;
		while (t)
		{
			++result;
			t &= t - 1;
		}
		return result;
	}

	queueFamilyIndices findQueueFamilies(vk::PhysicalDevice device) const
	{
		const auto queueFamilies = device.getQueueFamilyProperties();
		queueFamilyIndices indices;

		// Sort the families by the amount of capabilities supported
		std::vector<uint32_t> families(queueFamilies.size());
		std::iota(families.begin(), families.end(), 0);

		std::sort(families.begin(), families.end(), [&queueFamilies](const auto& p1, const auto& p2) {
			return popcnt(queueFamilies[p1].queueFlags) < popcnt(queueFamilies[p2].queueFlags);
		});

		if (![&]() -> bool {
				for (const auto& familyIndex : std::as_const(families))
				{
					if (queueFamilies[familyIndex].queueFlags & vk::QueueFlagBits::eGraphics && device.getSurfaceSupportKHR(familyIndex, surface))
					{
						indices.graphicsFamily = familyIndex;
						indices.presentFamily = familyIndex;
						return true;
					}
				}
				return false;
			}())
		{
			for (const auto& familyIndex : std::as_const(families))
			{
				if (queueFamilies[familyIndex].queueFlags & vk::QueueFlagBits::eGraphics)
				{
					indices.graphicsFamily = familyIndex;
				}
				if (device.getSurfaceSupportKHR(familyIndex, surface))
				{
					indices.presentFamily = familyIndex;
				}
			}
		}
		for (const auto& familyIndex : std::as_const(families))
		{
			if (queueFamilies[familyIndex].queueFlags & vk::QueueFlagBits::eTransfer)
			{
				indices.transferFamily = familyIndex;
				break;
			}
		}
		if (indices)
		{
			return indices;
		}
		return {};
	}

	static bool checkDeviceExtensionSupport(vk::PhysicalDevice device)
	{
		auto availableExtensions = device.enumerateDeviceExtensionProperties();

		return std::all_of(deviceExtensions.begin(), deviceExtensions.end(), [&availableExtensions](const std::string_view requiredExtension) {
			return std::find_if(availableExtensions.begin(), availableExtensions.end(), [&requiredExtension](vk::ExtensionProperties extension) {
				return requiredExtension == extension.extensionName;
			}) != availableExtensions.end();
		});
	}

	struct SwapChainSupportDetails
	{
		vk::SurfaceCapabilitiesKHR capabilities;
		std::vector<vk::SurfaceFormatKHR> formats;
		std::vector<vk::PresentModeKHR> presentModes;
	};

	SwapChainSupportDetails querySwapChainSupport(vk::PhysicalDevice physicalDevice) const
	{
		SwapChainSupportDetails details{
			physicalDevice.getSurfaceCapabilitiesKHR(surface),
			physicalDevice.getSurfaceFormatsKHR(surface),
			physicalDevice.getSurfacePresentModesKHR(surface)};

		return details;
	}

	bool isDeviceSuitable(vk::PhysicalDevice device) const
	{
		auto deviceFeatures = device.getFeatures();

		bool swapChainAppropriate = false;
		if (checkDeviceExtensionSupport(device))
		{
			SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
			swapChainAppropriate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
		}

		return swapChainAppropriate && findQueueFamilies(device) && deviceFeatures.samplerAnisotropy;
	}

	static vk::SurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats)
	{
		for (const auto& format : availableFormats)
		{
			if (format.format == vk::Format::eB8G8R8A8Srgb && format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
			{
				return format;
			}
		}
		return availableFormats[0];
	}

	static vk::PresentModeKHR chooseSwapPresentMode(const std::vector<vk::PresentModeKHR>& availableFormats)
	{
		if (std::find(availableFormats.begin(), availableFormats.end(), vk::PresentModeKHR::eMailbox) != availableFormats.end())
		{
			return vk::PresentModeKHR::eMailbox;
		}
		return vk::PresentModeKHR::eFifo;
	}

	vk::Extent2D chooseSwapExtent(const vk::SurfaceCapabilitiesKHR& capabilities)
	{
		if (capabilities.currentExtent.width != UINT32_MAX)
		{
			return capabilities.currentExtent;
		}

		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		vk::Extent2D actualExtent = {
			std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, static_cast<unsigned>(width))),
			std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.width, static_cast<unsigned>(height)))};
		return actualExtent;
	}

	vk::PhysicalDevice physicalDevice;
	void pickPhysicalDevice()
	{
		std::vector<vk::PhysicalDevice> devices = instance.enumeratePhysicalDevices();
		if (devices.size() == 0)
		{
			throw std::runtime_error("No physical device has Vulkan support!");
		}
		for (const auto& device : devices)
		{
			if (isDeviceSuitable(device))
			{
				queueIndices = findQueueFamilies(device);
				physicalDevice = std::move(device);
				break;
			}
		}
		if (!physicalDevice)
		{
			throw std::runtime_error("Failed to find a suitable physical device!");
		}
	}

	vk::Device device;
	vk::Queue graphicsQueue;
	vk::Queue presentQueue;
	vk::Queue transferQueue;
	void createLogicalDevice()
	{
		vk::PhysicalDeviceFeatures deviceFeatures{};
		deviceFeatures.samplerAnisotropy = VK_TRUE;
		std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;

		std::set<uint32_t> uniqueQueueFamilies = {queueIndices.graphicsFamily.value(), queueIndices.presentFamily.value(), queueIndices.transferFamily.value()};

		float queuePriority = 1.0f;
		for (uint32_t queueFamily : uniqueQueueFamilies)
		{
			queueCreateInfos.emplace_back(vk::DeviceQueueCreateInfo(
				{},
				queueFamily,
				1,
				&queuePriority));
		}
		vk::DeviceCreateInfo deviceCreateInfo({},
			queueCreateInfos,
			validationLayers,
			deviceExtensions,
			&deviceFeatures);
		device = physicalDevice.createDevice(deviceCreateInfo);
		VULKAN_HPP_DEFAULT_DISPATCHER.init(instance, device, dl);
		graphicsQueue = device.getQueue(queueIndices.graphicsFamily.value(), 0);
		presentQueue = device.getQueue(queueIndices.presentFamily.value(), 0);
		transferQueue = device.getQueue(queueIndices.transferFamily.value(), 0);
	}

	vk::SwapchainKHR swapChain;
	std::vector<vk::Image> swapChainImages;
	vk::Format swapChainFormat;
	vk::Extent2D swapChainExtent;
	void createSwapChain()
	{
		SwapChainSupportDetails swapChainDetails = querySwapChainSupport(physicalDevice);
		vk::SurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainDetails.formats);
		vk::PresentModeKHR presentMode = chooseSwapPresentMode(swapChainDetails.presentModes);
		vk::Extent2D extent = chooseSwapExtent(swapChainDetails.capabilities);
		uint32_t imageCount = swapChainDetails.capabilities.minImageCount + 1;
		if (swapChainDetails.capabilities.maxImageCount > 0 && imageCount > swapChainDetails.capabilities.maxImageCount)
		{
			imageCount = swapChainDetails.capabilities.maxImageCount;
		}
		const std::array queueFamilyIndices = {
			queueIndices.graphicsFamily.value(),
			queueIndices.presentFamily.value()};
		const bool singleQueueFamily = queueIndices.graphicsFamily == queueIndices.presentFamily;

		vk::SwapchainCreateInfoKHR createInfo(
			{},
			surface,
			imageCount,
			surfaceFormat.format,
			surfaceFormat.colorSpace,
			extent,
			1,
			vk::ImageUsageFlagBits::eColorAttachment,
			singleQueueFamily ? vk::SharingMode::eExclusive : vk::SharingMode::eConcurrent,
			{},
			swapChainDetails.capabilities.currentTransform,
			vk::CompositeAlphaFlagBitsKHR::eOpaque,
			presentMode,
			VK_TRUE,
			swapChain);
		if (!singleQueueFamily)
		{
			createInfo.setQueueFamilyIndices(queueFamilyIndices);
		}
		swapChain = device.createSwapchainKHR(createInfo);
		device.destroy(createInfo.oldSwapchain);
		swapChainImages = device.getSwapchainImagesKHR(swapChain);
		swapChainFormat = surfaceFormat.format;
		swapChainExtent = extent;
	}

	std::vector<vk::ImageView> swapChainImageViews;

	void createImageViews()
	{
		swapChainImageViews.resize(swapChainImages.size());
		for (size_t i = 0; i < swapChainImages.size(); ++i)
		{
			vk::ImageViewCreateInfo createInfo(
				{},
				swapChainImages[i],
				vk::ImageViewType::e2D,
				swapChainFormat,
				vk::ComponentMapping(
					vk::ComponentSwizzle::eIdentity,
					vk::ComponentSwizzle::eIdentity,
					vk::ComponentSwizzle::eIdentity,
					vk::ComponentSwizzle::eIdentity),
				vk::ImageSubresourceRange(
					vk::ImageAspectFlagBits::eColor,
					0,
					1,
					0,
					1));
			swapChainImageViews[i] = device.createImageView(createInfo);
		}
	}

	vk::ShaderModule createShaderModule(const std::string_view& code)
	{
		vk::ShaderModuleCreateInfo createInfo(
			{},
			code.size(),
			reinterpret_cast<const uint32_t*>(code.data()));
		vk::ShaderModule shaderModule = device.createShaderModule(createInfo);
		return shaderModule;
	}

	vk::RenderPass renderPass;
	void createRenderPass()
	{
		vk::AttachmentDescription colorAttachment(
			{},
			swapChainFormat,
			vk::SampleCountFlagBits::e1,
			vk::AttachmentLoadOp::eClear,
			vk::AttachmentStoreOp::eStore,
			vk::AttachmentLoadOp::eDontCare,
			vk::AttachmentStoreOp::eDontCare,
			vk::ImageLayout::eUndefined,
			vk::ImageLayout::ePresentSrcKHR);

		vk::AttachmentReference colorAttachmentRef(
			0,
			vk::ImageLayout::eColorAttachmentOptimal);

		vk::AttachmentDescription depthAttachment(
			{},
			findDepthFormat(),
			vk::SampleCountFlagBits::e1,
			vk::AttachmentLoadOp::eClear,
			vk::AttachmentStoreOp::eDontCare,
			vk::AttachmentLoadOp::eDontCare,
			vk::AttachmentStoreOp::eDontCare,
			vk::ImageLayout::eUndefined,
			vk::ImageLayout::eDepthStencilAttachmentOptimal);

		vk::AttachmentReference depthAttachmentRef(
			1,
			vk::ImageLayout::eDepthStencilAttachmentOptimal);

		vk::SubpassDescription subpass(
			{},
			vk::PipelineBindPoint::eGraphics,
			{},
			colorAttachmentRef,
			{},
			&depthAttachmentRef,
			{});

		std::array dependencies = {
			vk::SubpassDependency(
				VK_SUBPASS_EXTERNAL,
				0,
				vk::PipelineStageFlagBits::eColorAttachmentOutput,
				vk::PipelineStageFlagBits::eColorAttachmentOutput,
				{},
				vk::AccessFlagBits::eColorAttachmentWrite,
				vk::DependencyFlagBits::eByRegion),
			vk::SubpassDependency(
				VK_SUBPASS_EXTERNAL,
				0,
				vk::PipelineStageFlagBits::eLateFragmentTests,
				vk::PipelineStageFlagBits::eEarlyFragmentTests,
				{},
				vk::AccessFlagBits::eDepthStencilAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentRead,
				vk::DependencyFlagBits::eByRegion),
		};

		const std::array attachments =
			{
				colorAttachment,
				depthAttachment,
			};

		vk::RenderPassCreateInfo renderPassInfo(
			{},
			attachments,
			subpass,
			dependencies);

		renderPass = device.createRenderPass(renderPassInfo);
	}

	vk::DescriptorSetLayout descriptorSetLayout;
	void createDescriptorSetLayout()
	{
		vk::DescriptorSetLayoutBinding uboLayoutBinding(
			0,
			vk::DescriptorType::eUniformBuffer,
			1,
			vk::ShaderStageFlagBits::eVertex,
			{});

		vk::DescriptorSetLayoutBinding samplerLayoutBinding(
			1,
			vk::DescriptorType::eCombinedImageSampler,
			1,
			vk::ShaderStageFlagBits::eFragment,
			{});

		std::array bindings = {
			uboLayoutBinding,
			samplerLayoutBinding,
		};

		vk::DescriptorSetLayoutCreateInfo layoutInfo(
			{},
			bindings);

		descriptorSetLayout = device.createDescriptorSetLayout(layoutInfo);
	}

	vk::PipelineLayout pipelineLayout;
	vk::Pipeline graphicsPipeline;
	void createGraphicsPipeline()
	{
		vk::ShaderModule vertShaderModule = createShaderModule(readFile("basic.vert.spv"));
		vk::ShaderModule fragShaderModule = createShaderModule(readFile("basic.frag.spv"));

		vk::PipelineShaderStageCreateInfo vertShaderStageInfo(
			{},
			vk::ShaderStageFlagBits::eVertex,
			vertShaderModule,
			"main");
		vk::PipelineShaderStageCreateInfo fragShaderStageInfo(
			{},
			vk::ShaderStageFlagBits::eFragment,
			fragShaderModule,
			"main");
		const std::array shaderStages = {
			vertShaderStageInfo,
			fragShaderStageInfo};

		static constexpr auto attributeDescriptions = Vertex::getAttributeDescriptions();
		static constexpr std::array<vk::VertexInputBindingDescription, 1> bindingDescriptions = {
			Vertex::getBindingDescription() // FIXME: Vulkan.hpp freaks out trying to get a reference for this particular object, no idea why, packed it into a std::array, should be just as good.
		};

		vk::PipelineVertexInputStateCreateInfo vertexInputInfo(
			{},
			bindingDescriptions,
			attributeDescriptions);

		vk::PipelineInputAssemblyStateCreateInfo inputAssembly(
			{},
			vk::PrimitiveTopology::eTriangleList,
			VK_FALSE);

		vk::Viewport viewport(
			0.0f, 0.0f,
			swapChainExtent.width, swapChainExtent.height,
			0.0f, 1.0f);

		vk::Rect2D scissor({0, 0}, swapChainExtent);

		vk::PipelineViewportStateCreateInfo viewportState(
			{},
			viewport,
			scissor);

		vk::PipelineRasterizationStateCreateInfo rasterizer(
			{},
			VK_FALSE,
			VK_FALSE,
			vk::PolygonMode::eFill,
			vk::CullModeFlagBits::eBack,
			vk::FrontFace::eCounterClockwise,
			VK_FALSE,
			0.0f,
			0.0f,
			0.0f,
			1.0f);

		vk::PipelineMultisampleStateCreateInfo multisampling(
			{},
			vk::SampleCountFlagBits::e1,
			VK_FALSE,
			{},
			{},
			{},
			{});

		vk::PipelineDepthStencilStateCreateInfo depthStencil(
			{},
			VK_TRUE,
			VK_TRUE,
			vk::CompareOp::eGreater,
			VK_FALSE,
			VK_FALSE,
			{},
			{},
			{},
			{});

		vk::PipelineColorBlendAttachmentState colorBlendAttachment(
			VK_FALSE,
			vk::BlendFactor::eOne,
			vk::BlendFactor::eZero,
			vk::BlendOp::eAdd,
			vk::BlendFactor::eOne,
			vk::BlendFactor::eZero,
			vk::BlendOp::eAdd,
			vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA);

		vk::PipelineColorBlendStateCreateInfo colorBlending(
			{},
			VK_FALSE,
			vk::LogicOp::eCopy,
			colorBlendAttachment,
			{});

		std::array dynamicStates = {
			vk::DynamicState::eViewport,
		};

		vk::PipelineDynamicStateCreateInfo dynamicState(
			{},
			dynamicStates);

		vk::PipelineLayoutCreateInfo pipelineLayoutInfo(
			{},
			descriptorSetLayout,
			{});

		pipelineLayout = device.createPipelineLayout(pipelineLayoutInfo);

		vk::GraphicsPipelineCreateInfo pipelineInfo(
			{},
			shaderStages,
			&vertexInputInfo,
			&inputAssembly,
			{},
			&viewportState,
			&rasterizer,
			&multisampling,
			&depthStencil,
			&colorBlending,
			{},
			pipelineLayout,
			renderPass,
			0,
			{},
			{});

		graphicsPipeline = device.createGraphicsPipeline({}, pipelineInfo).value;
		device.destroy(fragShaderModule);
		device.destroy(vertShaderModule);
	}

	std::vector<vk::Framebuffer> swapChainFramebuffers;
	void createFramebuffers()
	{
		swapChainFramebuffers.resize(swapChainImageViews.size());
		for (size_t i = 0; i < swapChainImageViews.size(); ++i)
		{
			std::array attachments = {
				swapChainImageViews[i],
				depthImageView};

			vk::FramebufferCreateInfo framebufferInfo(
				{},
				renderPass,
				attachments,
				swapChainExtent.width,
				swapChainExtent.height,
				1);
			swapChainFramebuffers[i] = device.createFramebuffer(framebufferInfo);
		}
	}

	vk::CommandPool commandPool;
	void createCommandPool()
	{
		vk::CommandPoolCreateInfo poolInfo(
			vk::CommandPoolCreateFlagBits::eTransient | vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
			queueIndices.graphicsFamily.value());
		commandPool = device.createCommandPool(poolInfo);
	}

	uint32_t findMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties)
	{
		vk::PhysicalDeviceMemoryProperties memProperties = physicalDevice.getMemoryProperties();
		for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i)
		{
			if (typeFilter & (1 << i) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
			{
				return i;
			}
		}
		throw std::runtime_error("failed to find suitable memory type!");
	}

	vk::DeviceMemory allocateBufferMemory(const vk::Buffer& buffer, vk::MemoryPropertyFlags properties)
	{
		vk::MemoryRequirements memRequirements = device.getBufferMemoryRequirements(buffer);
		vk::StructureChain allocInfoChain = {
			vk::MemoryAllocateInfo(memRequirements.size, findMemoryType(memRequirements.memoryTypeBits, properties)),
			vk::MemoryDedicatedAllocateInfo({}, buffer)};
		vk::DeviceMemory memory = device.allocateMemory(std::get<0>(allocInfoChain));
		device.bindBufferMemory(buffer, memory, 0);
		return memory;
	}

	vk::DeviceMemory allocateImageMemory(const vk::Image& image, vk::MemoryPropertyFlags properties)
	{
		vk::MemoryRequirements memRequirements = device.getImageMemoryRequirements(image);
		vk::StructureChain allocInfoChain = {
			vk::MemoryAllocateInfo(memRequirements.size, findMemoryType(memRequirements.memoryTypeBits, properties)),
			vk::MemoryDedicatedAllocateInfo(image, {})};
		vk::DeviceMemory memory = device.allocateMemory(std::get<0>(allocInfoChain));
		device.bindImageMemory(image, memory, 0);
		return memory;
	}

	void copyBuffer(vk::Buffer& srcBuffer, vk::Buffer& dstBuffer, vk::DeviceSize size)
	{
		vk::CommandPool tmpCommandPool = device.createCommandPool(vk::CommandPoolCreateInfo(
			vk::CommandPoolCreateFlagBits::eTransient,
			queueIndices.transferFamily.value()));

		vk::CommandBuffer tmpCommandBuffer = device.allocateCommandBuffers(vk::CommandBufferAllocateInfo(
			tmpCommandPool,
			vk::CommandBufferLevel::ePrimary,
			1))[0];
		tmpCommandBuffer.begin(vk::CommandBufferBeginInfo(
			vk::CommandBufferUsageFlagBits::eOneTimeSubmit,
			{}));
		vk::BufferCopy copyRegion(
			0,
			0,
			size);
		tmpCommandBuffer.copyBuffer(srcBuffer, dstBuffer, copyRegion);
		tmpCommandBuffer.end();

		vk::SubmitInfo submitInfo(
			{},
			{},
			tmpCommandBuffer,
			{});

		transferQueue.submit(submitInfo, {});
		transferQueue.waitIdle();

		device.free(tmpCommandPool, tmpCommandBuffer);
		device.destroy(tmpCommandPool);
	}

	void transitionImageLayout(vk::Image& image, vk::Format format, vk::ImageLayout oldLayout, vk::ImageLayout newLayout)
	{
		vk::PipelineStageFlags sourceStage;
		vk::PipelineStageFlags destinationStage;

		vk::AccessFlags sourceAccessMask;
		vk::AccessFlags destinationAccessMask;

		if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eTransferDstOptimal)
		{
			sourceAccessMask = {};
			destinationAccessMask = vk::AccessFlagBits::eTransferWrite;

			sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
			destinationStage = vk::PipelineStageFlagBits::eTransfer;
		}
		else if (oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal)
		{
			sourceAccessMask = vk::AccessFlagBits::eTransferWrite;
			destinationAccessMask = vk::AccessFlagBits::eShaderRead;

			sourceStage = vk::PipelineStageFlagBits::eTransfer;
			destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
		}
		else
		{
			throw std::runtime_error("Unsupported layout transition!");
		}

		vk::CommandPool tmpCommandPool = device.createCommandPool(vk::CommandPoolCreateInfo(
			vk::CommandPoolCreateFlagBits::eTransient,
			queueIndices.graphicsFamily.value()));
		vk::CommandBuffer tmpCommandBuffer = device.allocateCommandBuffers(vk::CommandBufferAllocateInfo(
			tmpCommandPool,
			vk::CommandBufferLevel::ePrimary,
			1))[0];
		tmpCommandBuffer.begin(vk::CommandBufferBeginInfo(
			vk::CommandBufferUsageFlagBits::eOneTimeSubmit,
			{}));
		vk::ImageAspectFlags aspectFlags{};
		switch (newLayout)
		{
		default:
			aspectFlags = vk::ImageAspectFlagBits::eColor;
			break;
		}
		vk::ImageMemoryBarrier barrier(
			sourceAccessMask,
			destinationAccessMask,
			oldLayout,
			newLayout,
			VK_QUEUE_FAMILY_IGNORED,
			VK_QUEUE_FAMILY_IGNORED,
			image,
			vk::ImageSubresourceRange(
				aspectFlags,
				0,
				1,
				0,
				1));
		tmpCommandBuffer.pipelineBarrier(sourceStage, destinationStage, {}, {}, {}, barrier);
		tmpCommandBuffer.end();

		graphicsQueue.submit(vk::SubmitInfo({}, {}, tmpCommandBuffer, {}), {});
		graphicsQueue.waitIdle();

		device.free(tmpCommandPool, tmpCommandBuffer);
		device.destroy(tmpCommandPool);
	}

	void copyBufferToImage(vk::Buffer& srcBuffer, vk::Image& dstImage, uint32_t width, uint32_t height)
	{
		vk::CommandPool tmpCommandPool = device.createCommandPool(vk::CommandPoolCreateInfo(
			vk::CommandPoolCreateFlagBits::eTransient,
			queueIndices.transferFamily.value()));
		vk::CommandBuffer tmpCommandBuffer = device.allocateCommandBuffers(vk::CommandBufferAllocateInfo(
			tmpCommandPool,
			vk::CommandBufferLevel::ePrimary,
			1))[0];
		tmpCommandBuffer.begin(vk::CommandBufferBeginInfo(
			vk::CommandBufferUsageFlagBits::eOneTimeSubmit,
			{}));
		vk::BufferImageCopy region(
			0,
			0,
			0,
			vk::ImageSubresourceLayers(
				vk::ImageAspectFlagBits::eColor,
				0,
				0,
				1),
			vk::Offset3D(0, 0, 0),
			vk::Extent3D(width, height, 1));
		tmpCommandBuffer.copyBufferToImage(srcBuffer, dstImage, vk::ImageLayout::eTransferDstOptimal, region);
		tmpCommandBuffer.end();

		transferQueue.submit(vk::SubmitInfo({}, {}, tmpCommandBuffer, {}), {});
		transferQueue.waitIdle();

		device.free(tmpCommandPool, tmpCommandBuffer);
		device.destroy(tmpCommandPool);
	}

	vk::Buffer vertexBuffer;
	vk::DeviceMemory vertexBufferMemory;
	void createVertexBuffer()
	{
		vk::DeviceSize bufferSize = sizeof(Vertex) * vertices.size();
		vk::Buffer stagingBuffer = device.createBuffer(vk::BufferCreateInfo(
			{},
			bufferSize,
			vk::BufferUsageFlagBits::eTransferSrc,
			vk::SharingMode::eExclusive,
			{}));
		vk::DeviceMemory stagingBufferMemory = allocateBufferMemory(stagingBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

		void* data = device.mapMemory(stagingBufferMemory, 0, bufferSize, {});
		std::memcpy(data, vertices.data(), bufferSize);
		device.unmapMemory(stagingBufferMemory);

		vertexBuffer = device.createBuffer(vk::BufferCreateInfo(
			{},
			bufferSize,
			vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eTransferDst,
			vk::SharingMode::eExclusive,
			{}));
		vertexBufferMemory = allocateBufferMemory(vertexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);

		copyBuffer(stagingBuffer, vertexBuffer, bufferSize);

		device.free(stagingBufferMemory);
		device.destroy(stagingBuffer);
	}

	vk::Buffer indexBuffer;
	vk::DeviceMemory indexBufferMemory;
	void createIndexBuffer()
	{
		vk::DeviceSize bufferSize = sizeof(indices[0]) * indices.size();
		vk::Buffer stagingBuffer = device.createBuffer(vk::BufferCreateInfo(
			{},
			bufferSize,
			vk::BufferUsageFlagBits::eTransferSrc,
			vk::SharingMode::eExclusive,
			{}));
		vk::DeviceMemory stagingBufferMemory = allocateBufferMemory(stagingBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

		void* data = device.mapMemory(stagingBufferMemory, 0, bufferSize, {});
		std::memcpy(data, indices.data(), bufferSize);
		device.unmapMemory(stagingBufferMemory);

		indexBuffer = device.createBuffer(vk::BufferCreateInfo(
			{},
			bufferSize,
			vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferDst,
			vk::SharingMode::eExclusive,
			{}));
		indexBufferMemory = allocateBufferMemory(indexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);

		copyBuffer(stagingBuffer, indexBuffer, bufferSize);

		device.free(stagingBufferMemory);
		device.destroy(stagingBuffer);
	}

	vk::Format findSupportedFormat(const std::vector<vk::Format>& candidates, vk::ImageTiling imageTiling, vk::FormatFeatureFlags features)
	{
		for (const auto& format : candidates)
		{
			vk::FormatProperties props = physicalDevice.getFormatProperties(format);
			if (imageTiling == vk::ImageTiling::eLinear && (props.linearTilingFeatures & features) == features)
			{
				return format;
			}
			if (imageTiling == vk::ImageTiling::eOptimal && (props.optimalTilingFeatures & features) == features)
			{
				return format;
			}
		}
		throw std::runtime_error("No viable depth image format found!");
	}

	bool hasStencilComponent(vk::Format format)
	{
		return format == vk::Format::eD32SfloatS8Uint || format == vk::Format::eD24UnormS8Uint;
	}

	vk::Format findDepthFormat()
	{
		return findSupportedFormat(
			{vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint},
			vk::ImageTiling::eOptimal,
			vk::FormatFeatureFlagBits::eDepthStencilAttachment);
	}

	vk::Image depthImage;
	vk::DeviceMemory depthImageMemory;
	vk::ImageView depthImageView;
	void createDepthResources()
	{
		vk::Format depthFormat = findDepthFormat();
		depthImage = device.createImage(vk::ImageCreateInfo(
			{},
			vk::ImageType::e2D,
			depthFormat,
			vk::Extent3D(swapChainExtent, 1),
			1,
			1,
			vk::SampleCountFlagBits::e1,
			vk::ImageTiling::eOptimal,
			vk::ImageUsageFlagBits::eDepthStencilAttachment,
			vk::SharingMode::eExclusive,
			{},
			vk::ImageLayout::eUndefined));
		depthImageMemory = allocateImageMemory(depthImage, vk::MemoryPropertyFlagBits::eDeviceLocal);
		depthImageView = device.createImageView(vk::ImageViewCreateInfo(
			{},
			depthImage,
			vk::ImageViewType::e2D,
			depthFormat,
			vk::ComponentMapping(),
			vk::ImageSubresourceRange(
				vk::ImageAspectFlagBits::eDepth,
				0,
				1,
				0,
				1)));
	}

	vk::Image textureImage;
	vk::DeviceMemory textureImageMemory;
	void createTextureImage()
	{
		int texWidth, texHeight, texChannels;
		auto* pixels = stbi_load("texture.jpg", &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
		vk::DeviceSize imageSize = texWidth * texHeight * 4;

		vk::Buffer stagingBuffer = device.createBuffer(vk::BufferCreateInfo(
			{},
			imageSize,
			vk::BufferUsageFlagBits::eTransferSrc,
			{}));

		vk::DeviceMemory stagingBufferMemory = allocateBufferMemory(stagingBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
		void* data = device.mapMemory(stagingBufferMemory, 0, imageSize);
		std::memcpy(data, pixels, imageSize);
		device.unmapMemory(stagingBufferMemory);
		stbi_image_free(pixels);

		textureImage = device.createImage(vk::ImageCreateInfo(
			{},
			vk::ImageType::e2D,
			vk::Format::eR8G8B8A8Srgb,
			vk::Extent3D(texWidth, texHeight, 1),
			1,
			1,
			vk::SampleCountFlagBits::e1,
			vk::ImageTiling::eOptimal,
			vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled,
			vk::SharingMode::eExclusive,
			{},
			vk::ImageLayout::eUndefined));
		textureImageMemory = allocateImageMemory(textureImage, vk::MemoryPropertyFlagBits::eDeviceLocal);
		transitionImageLayout(textureImage, vk::Format::eR8G8B8A8Srgb, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal);
		copyBufferToImage(stagingBuffer, textureImage, texWidth, texHeight);

		device.free(stagingBufferMemory);
		device.destroy(stagingBuffer);

		transitionImageLayout(textureImage, vk::Format::eR8G8B8A8Srgb, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);
	}

	vk::ImageView textureImageView;
	void createTextureImageView()
	{
		textureImageView = device.createImageView(vk::ImageViewCreateInfo(
			{},
			textureImage,
			vk::ImageViewType::e2D,
			vk::Format::eR8G8B8A8Srgb,
			{},
			vk::ImageSubresourceRange(
				vk::ImageAspectFlagBits::eColor,
				0,
				1,
				0,
				1)));
	}

	vk::Sampler textureSampler;
	void createTextureSampler()
	{
		vk::SamplerCreateInfo samplerInfo(
			{},
			vk::Filter::eLinear,
			vk::Filter::eLinear,
			vk::SamplerMipmapMode::eLinear,
			vk::SamplerAddressMode::eMirroredRepeat,
			vk::SamplerAddressMode::eMirroredRepeat,
			vk::SamplerAddressMode::eMirroredRepeat,
			0.0f,
			VK_TRUE,
			16.0f,
			VK_FALSE,
			vk::CompareOp::eAlways,
			0.0f,
			0.0f,
			vk::BorderColor::eIntOpaqueBlack,
			VK_FALSE);
		textureSampler = device.createSampler(samplerInfo);
	}

	std::vector<vk::Buffer> uniformBuffers;
	std::vector<vk::DeviceMemory> uniformBuffersMemory;
	void createUniformBuffers()
	{
		vk::DeviceSize bufferSize = sizeof(UniformBufferObject);
		uniformBuffers.resize(swapChainImages.size());
		uniformBuffersMemory.resize(swapChainImages.size());

		for (size_t i = 0; i < swapChainImages.size(); ++i)
		{
			uniformBuffers[i] = device.createBuffer(vk::BufferCreateInfo(
				{},
				bufferSize,
				vk::BufferUsageFlagBits::eUniformBuffer,
				vk::SharingMode::eExclusive,
				{}));
			uniformBuffersMemory[i] = allocateBufferMemory(uniformBuffers[i],
				vk::MemoryPropertyFlagBits::eDeviceLocal |
					vk::MemoryPropertyFlagBits::eHostVisible |
					vk::MemoryPropertyFlagBits::eHostCoherent);
		}
	}

	vk::DescriptorPool descriptorPool;
	void createDescriptorPool()
	{
		std::array poolSizes{
			vk::DescriptorPoolSize(
				vk::DescriptorType::eUniformBuffer,
				swapChainImages.size()),
			vk::DescriptorPoolSize(
				vk::DescriptorType::eCombinedImageSampler,
				swapChainImages.size()),
		};

		vk::DescriptorPoolCreateInfo poolInfo(
			{},
			swapChainImages.size(),
			poolSizes);
		descriptorPool = device.createDescriptorPool(poolInfo);
	}

	std::vector<vk::DescriptorSet> descriptorSets;
	void createDescriptorSets()
	{
		std::vector<vk::DescriptorSetLayout> layouts(swapChainImages.size(), descriptorSetLayout);
		vk::DescriptorSetAllocateInfo allocInfo(
			descriptorPool,
			layouts);
		descriptorSets = device.allocateDescriptorSets(allocInfo);

		for (size_t i = 0; i < swapChainImages.size(); ++i)
		{
			vk::DescriptorBufferInfo bufferInfo(
				uniformBuffers[i],
				0,
				sizeof(UniformBufferObject));

			vk::DescriptorImageInfo imageInfo(
				textureSampler,
				textureImageView,
				vk::ImageLayout::eShaderReadOnlyOptimal);

			std::array descriptorWrites = {
				vk::WriteDescriptorSet(
					descriptorSets[i],
					0,
					0,
					vk::DescriptorType::eUniformBuffer,
					{},
					bufferInfo,
					{}),
				vk::WriteDescriptorSet(
					descriptorSets[i],
					1,
					0,
					vk::DescriptorType::eCombinedImageSampler,
					imageInfo,
					{},
					{}),
			};

			device.updateDescriptorSets(descriptorWrites, {});
		}
	}

	std::vector<vk::CommandBuffer> commandBuffers;
	void createCommandBuffers()
	{
		commandBuffers.resize(swapChainFramebuffers.size());
		vk::CommandBufferAllocateInfo allocInfo(
			commandPool,
			vk::CommandBufferLevel::ePrimary,
			commandBuffers.size());

		commandBuffers = device.allocateCommandBuffers(allocInfo);
	}

	static constexpr int MAX_FRAMES_IN_FLIGHT = 2;

	std::vector<vk::Semaphore> imageAvailableSemaphores;
	std::vector<vk::Semaphore> renderFinishedSemaphores;
	std::vector<vk::Fence> inFlightFences;
	void createSyncObjects()
	{
		imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
		renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
		inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

		vk::StructureChain semaphoreInfo = {
			vk::SemaphoreCreateInfo{},
			vk::SemaphoreTypeCreateInfo{vk::SemaphoreType::eBinary},
		};

		vk::FenceCreateInfo fenceInfo(
			vk::FenceCreateFlagBits::eSignaled);

		for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
		{
			imageAvailableSemaphores[i] = device.createSemaphore(std::get<0>(semaphoreInfo));
			renderFinishedSemaphores[i] = device.createSemaphore(std::get<0>(semaphoreInfo));
			inFlightFences[i] = device.createFence(fenceInfo);
		}
	}

	void initVulkan()
	{
		createInstance();
		setupDebugMessenger();
		createSurface();
		pickPhysicalDevice();
		createLogicalDevice();
		createSwapChain();
		createImageViews();
		createRenderPass();
		createDescriptorSetLayout();
		createGraphicsPipeline();
		createDepthResources();
		createFramebuffers();
		createCommandPool();
		createVertexBuffer();
		createIndexBuffer();
		createTextureImage();
		createTextureImageView();
		createTextureSampler();
		createUniformBuffers();
		createDescriptorPool();
		createDescriptorSets();
		createCommandBuffers();
		createSyncObjects();
	}

	void cleanupSwapChain()
	{
		for (auto& framebuffer : swapChainFramebuffers)
		{
			device.destroy(framebuffer);
		}
		device.free(commandPool, commandBuffers);

		device.destroy(depthImage);
		device.free(depthImageMemory);
		device.destroy(depthImageView);

		device.destroy(graphicsPipeline);
		device.destroy(pipelineLayout);
		device.destroy(renderPass);

		for (auto& imageView : swapChainImageViews)
		{
			device.destroy(imageView);
		}

		for (auto& uniformBuffer : uniformBuffers)
		{
			device.destroy(uniformBuffer);
		}
		for (auto& uniformBufferMemory : uniformBuffersMemory)
		{
			device.free(uniformBufferMemory);
		}

		device.destroy(descriptorPool);
	}

	void recreateSwapChain()
	{
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		while (width == 0 || height == 0)
		{
			glfwGetFramebufferSize(window, &width, &height);
			glfwWaitEvents();
		}

		device.waitIdle();

		cleanupSwapChain();

		createSwapChain();
		createImageViews();
		createRenderPass();
		createGraphicsPipeline();
		createDepthResources();
		createFramebuffers();
		createUniformBuffers();
		createDescriptorPool();
		createDescriptorSets();
		createCommandBuffers();
	}

	void updateUniformBuffer(uint32_t currentImage)
	{
		double time = std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1, 1>>>(std::chrono::steady_clock::now().time_since_epoch()).count();
		UniformBufferObject ubo{};
		ubo.model = glm::rotate<float>(glm::mat4(1), time * glm::radians(360.0f), glm::vec3(0, 0, 1));
		ubo.view = glm::lookAt(glm::vec3(0.5f, 0.5f, 0.2f), glm::vec3(0.0f, 0.0f, -0.4f), glm::vec3(0.0f, 0.0f, 1.0f)),
		ubo.projection = [](auto fovy, auto aspect, auto znear) {
			float f = 1.0f / tan(fovy / 2.0f);
			return glm::mat4(
				f / aspect, 0, 0, 0,
				0, -f, 0, 0,
				0, 0, 0, -1.0f,
				0, 0, znear, 0);
		}(glm::radians(90.0f), swapChainExtent.width / static_cast<float>(swapChainExtent.height), 1.0f / 16);
		void* data = device.mapMemory(uniformBuffersMemory[currentImage], 0, sizeof(UniformBufferObject));
		std::memcpy(data, &ubo, sizeof(UniformBufferObject));
		device.unmapMemory(uniformBuffersMemory[currentImage]);
	}

	size_t currentFrame = 0;
	bool frameBufferResized = false;
	void drawFrame()
	{
		uint32_t imageIndex;
		try
		{
			auto result = device.acquireNextImageKHR(swapChain, UINT64_MAX, imageAvailableSemaphores[currentFrame], {});
			if (result.result == vk::Result::eSuboptimalKHR)
			{
				recreateSwapChain();
				return;
			}
			imageIndex = result.value;
		}
		catch (const vk::OutOfDateKHRError& e)
		{
			recreateSwapChain();
			return;
		}

		device.waitForFences(inFlightFences[currentFrame], VK_TRUE, UINT64_MAX);

		updateUniformBuffer(currentFrame);

		vk::CommandBuffer& commandBuffer = commandBuffers[currentFrame];

		commandBuffer.reset({});

		vk::CommandBufferBeginInfo beginInfo(
			vk::CommandBufferUsageFlagBits::eOneTimeSubmit,
			{});
		commandBuffer.begin(beginInfo);

		std::array clearValues = {
			vk::ClearValue(std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f}),
			vk::ClearValue(vk::ClearDepthStencilValue(0.0f, 0.0f))};

		vk::RenderPassBeginInfo renderPassInfo(
			renderPass,
			swapChainFramebuffers[imageIndex],
			{
				{0, 0},
				swapChainExtent
		   },
			clearValues);
		int width, height;
		glfwGetWindowSize(window, &width, &height);

		// 		commandBuffer.setViewport(0, vk::Viewport(0, 0, width, height));
		commandBuffer.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
		commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, graphicsPipeline);
		commandBuffer.bindVertexBuffers(0, vertexBuffer, {0});
		commandBuffer.bindIndexBuffer(indexBuffer, 0, vk::IndexType::eUint32);
		commandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, pipelineLayout, 0, descriptorSets[currentFrame], {});
		commandBuffer.drawIndexed(indices.size(), 1, 0, 0, 0);
		commandBuffer.endRenderPass();
		commandBuffer.end();

		std::array waitStages = {
			vk::PipelineStageFlags(vk::PipelineStageFlagBits::eColorAttachmentOutput),
		};

		vk::SubmitInfo submitInfo(
			imageAvailableSemaphores[currentFrame],
			waitStages,
			commandBuffer,
			renderFinishedSemaphores[currentFrame]);
		device.resetFences(inFlightFences[currentFrame]);
		graphicsQueue.submit(submitInfo, inFlightFences[currentFrame]);

		vk::PresentInfoKHR presentInfo(
			renderFinishedSemaphores[currentFrame],
			swapChain,
			imageIndex,
			{});
		try
		{
			if (presentQueue.presentKHR(presentInfo) == vk::Result::eSuboptimalKHR || frameBufferResized)
			{
				frameBufferResized = false;
				recreateSwapChain();
			}
		}
		catch (const vk::OutOfDateKHRError& e)
		{
			recreateSwapChain();
		}
		currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
	}

	void mainLoop()
	{
		while (!glfwWindowShouldClose(window))
		{
			glfwPollEvents();
			drawFrame();
		}
		device.waitIdle();
	}
	void cleanup()
	{
		cleanupSwapChain();

		device.destroy(descriptorSetLayout);

		device.destroy(textureSampler);

		device.destroy(textureImageView);

		device.destroy(textureImage);
		device.free(textureImageMemory);

		device.destroy(indexBuffer);
		device.free(indexBufferMemory);

		device.destroy(vertexBuffer);
		device.free(vertexBufferMemory);

		for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
		{
			device.destroy(renderFinishedSemaphores[i]);
			device.destroy(imageAvailableSemaphores[i]);
			device.destroy(inFlightFences[i]);
		}
		device.destroy(commandPool);
		device.destroy(swapChain);
		device.destroy();
#ifndef NDEBUG
		instance.destroyDebugUtilsMessengerEXT(debugMessenger);
#endif
		instance.destroy(surface);
		instance.destroy();
		glfwDestroyWindow(window);
		glfwTerminate();
	}

public:
	void run()
	{
		initWindow();
		initVulkan();
		mainLoop();
		cleanup();
	}
};

int main(int argc, char** argv)
{
	HelloTriangle app;
	app.run();
	return 0;
}
